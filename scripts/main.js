// Titel Zeile stylen
const elHeaderRow = document.querySelector('thead tr');
elHeaderRow.classList.add('darkblue');

// Orte in erster Spalte stylen
const elEvenRows = document.querySelectorAll('tbody td:nth-child(1)');
for (let i = 0; i < elEvenRows.length; i++) {
  let elEvenRow = elEvenRows[i];
  elEvenRow.classList.add('city');
}

// grösste Werte stylen
// alle td-Elemente in 2. Zeile selektieren
const elBiggestValues = document.querySelectorAll('tbody tr:nth-child(2) td');
// alle td-Elemente ausser 1. mit Ortschaft stylen
// => Laufvariable startet mit 1 (Index für 2. td-Element)
for (let i = 1; i < elBiggestValues.length; i++) {
  const elBiggestValue = elBiggestValues[i];
  elBiggestValue.classList.add('biggest');
}

// kleinste Werte stylen
const elSmallestValues = document.querySelectorAll(
  'tbody tr:nth-child(3) td:nth-child(2), ' + // 3. Zeile, 2. Spalte
    'tbody tr:nth-child(3) td:nth-child(3), ' + // 3. Zeile, 3. Spalte
    'tbody tr:nth-child(4) td:nth-child(4), ' + // 4. Zeile, 4. Spalte
    'tbody tr:nth-child(4) td:nth-child(5), ' + // 4. Zeile, 5. Spalte
    'tbody tr:nth-child(4) td:nth-child(6)' // 4. Zeile, 6. Spalte
);
for (let i = 0; i < elSmallestValues.length; i++) {
  const elSmallestValue = elSmallestValues[i];
  elSmallestValue.classList.add('smallest');
}

